FROM maven:3.9.6-eclipse-temurin-21-alpine AS build
COPY src /home/app/src
COPY pom.xml /home/app
COPY entrypoint.sh /entrypoint.sh
RUN apk add --no-cache curl
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
