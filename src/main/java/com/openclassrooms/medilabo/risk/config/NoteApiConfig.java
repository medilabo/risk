package com.openclassrooms.medilabo.risk.config;

import com.openclassrooms.medilabo.note.api.NoteApi;
import com.openclassrooms.medilabo.note.client.ApiClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class NoteApiConfig {

    @Bean
    NoteApi noteApi() {
        return new NoteApi(new ApiClient());
    }
}
