package com.openclassrooms.medilabo.risk.config;

import com.openclassrooms.medilabo.patient.api.PatientApi;
import com.openclassrooms.medilabo.patient.client.ApiClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class PatientApiConfig {

    @Bean
    PatientApi patientApi() {
        return new PatientApi(new ApiClient());
    }
}
