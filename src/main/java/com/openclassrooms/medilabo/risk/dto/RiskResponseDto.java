package com.openclassrooms.medilabo.risk.dto;

import com.openclassrooms.medilabo.risk.model.RiskEnum;
import io.swagger.v3.oas.annotations.media.Schema;

@Schema(name = "RiskResponse")
public record RiskResponseDto(
        RiskEnum risk
){
}
