package com.openclassrooms.medilabo.risk.model;

public enum GenderEnum {
    M,
    F
}
