package com.openclassrooms.medilabo.risk.model;

public enum RiskEnum {
    NONE,
    BORDERLINE,
    IN_DANGER,
    EARLY_ONSET
}
