package com.openclassrooms.medilabo.risk.controller;

import com.openclassrooms.medilabo.risk.dto.RiskResponseDto;
import com.openclassrooms.medilabo.risk.model.RiskEnum;
import com.openclassrooms.medilabo.risk.service.business.RiskService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@SecurityRequirement(name = "bearerAuth")
@Tag(name = "Risk")
public class RiskController {

    private final RiskService riskService;

    @GetMapping("/patients/{id}/risks")
    @Operation(operationId = "getRisk")
    public ResponseEntity<RiskResponseDto> getRisk(
            @PathVariable Long id
    ) {
        RiskEnum risk = riskService.getRisk(id);
        return ResponseEntity.ok(new RiskResponseDto(risk));
    }
}
