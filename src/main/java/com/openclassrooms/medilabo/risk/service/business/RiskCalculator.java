package com.openclassrooms.medilabo.risk.service.business;

import com.openclassrooms.medilabo.risk.model.GenderEnum;
import com.openclassrooms.medilabo.risk.model.RiskEnum;

public class RiskCalculator {

    public static RiskEnum calculate(int age, GenderEnum genderEnum, Integer nbTriggerTerms) {
        if (nbTriggerTerms < 2) {
            return RiskEnum.NONE;
        }
        if (age > 30) {
            if (nbTriggerTerms >= 8) return RiskEnum.EARLY_ONSET;
            if (nbTriggerTerms >= 6) return RiskEnum.IN_DANGER;
            return RiskEnum.BORDERLINE;
        }
        if (genderEnum.equals(GenderEnum.M)) {
            if (nbTriggerTerms >= 5) return RiskEnum.EARLY_ONSET;
            if (nbTriggerTerms >= 3) return RiskEnum.IN_DANGER;
        } else {
            if (nbTriggerTerms >= 7) return RiskEnum.EARLY_ONSET;
            if (nbTriggerTerms >= 4) return RiskEnum.IN_DANGER;
        }
        return RiskEnum.NONE;
    }
}
