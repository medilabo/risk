package com.openclassrooms.medilabo.risk.service.business;

import com.openclassrooms.medilabo.note.api.NoteApi;
import com.openclassrooms.medilabo.patient.api.PatientApi;
import com.openclassrooms.medilabo.patient.model.PatientResponse;
import com.openclassrooms.medilabo.risk.model.GenderEnum;
import com.openclassrooms.medilabo.risk.model.RiskEnum;
import com.openclassrooms.medilabo.risk.service.tech.NoteApiService;
import com.openclassrooms.medilabo.risk.service.tech.PatientApiService;
import com.openclassrooms.medilabo.risk.utils.DateUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class RiskService {

    private final NoteApiService noteApiService;

    private final PatientApiService patientApiService;

    public RiskEnum getRisk(Long patientId) {
        PatientResponse patient = patientApiService.getPatient(patientId);
        List<String> notes = noteApiService.getNotes(patientId);
        int age = DateUtils.getAge(patient.getBirthday());
        Integer nbTriggerTerms = TriggerTermsCalculator.calculate(notes);
        return RiskCalculator.calculate(age, GenderEnum.valueOf(patient.getGender()), nbTriggerTerms);
    }
}
