package com.openclassrooms.medilabo.risk.service.tech;

import com.openclassrooms.medilabo.note.api.NoteApi;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class NoteApiService {

    private final NoteApi noteApi;

    public List<String> getNotes(Long patientId) {
        setBearerToken();
        return noteApi.getNotes(patientId);
    }

    private void setBearerToken() {
        String token = BearerTokenUtils.getBearerToken();
        noteApi.getApiClient().setBearerToken(token);
    }
}
