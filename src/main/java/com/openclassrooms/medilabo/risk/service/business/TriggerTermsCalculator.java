package com.openclassrooms.medilabo.risk.service.business;

import org.springframework.util.StringUtils;

import java.util.List;

public class TriggerTermsCalculator {

    private static final List<String> TRIGGER_TERMS = List.of(
            "Hémoglobine A1C",
            "Microalbumine",
            "Taille",
            "Poids",
            "Fumeur", "Fumeuse",
            "Anormal",
            "Cholestérol",
            "Vertiges",
            "Rechute",
            "Réaction",
            "Anticorps"
    );

    public static Integer calculate(List<String> notes) {
        return notes.stream()
                .mapToInt(note -> {
                    String noteLower = note.toLowerCase();
                    return (int) TRIGGER_TERMS.stream()
                            .filter(term -> noteLower.contains(term.toLowerCase()))
                            .distinct()
                            .count();
                }).sum();
    }
}
