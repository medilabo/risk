package com.openclassrooms.medilabo.risk.service.tech;

import com.openclassrooms.medilabo.patient.api.PatientApi;
import com.openclassrooms.medilabo.patient.model.PatientResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PatientApiService {

    private final PatientApi patientApi;

    public PatientResponse getPatient(Long patientId) {
        setBearerToken();
        return patientApi.getPatient(patientId);
    }

    private void setBearerToken() {
        String token = BearerTokenUtils.getBearerToken();
        patientApi.getApiClient().setBearerToken(token);
    }
}
