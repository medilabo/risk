package com.openclassrooms.medilabo.risk.UT.service;

import com.openclassrooms.medilabo.risk.model.GenderEnum;
import com.openclassrooms.medilabo.risk.model.RiskEnum;
import com.openclassrooms.medilabo.risk.service.business.RiskCalculator;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.assertj.core.api.Assertions.assertThat;

class RiskCalculatorTest {

    @ParameterizedTest
    @CsvSource({
            "NONE, 31, F, 1",
            "NONE, 31, M, 1",
            "EARLY_ONSET, 31, F, 8",
            "EARLY_ONSET, 31, M, 8",
            "IN_DANGER, 31, F, 6",
            "IN_DANGER, 31, M, 6",
            "BORDERLINE, 31, F, 5",
            "BORDERLINE, 31, M, 5",
            "EARLY_ONSET, 30, M, 5",
            "IN_DANGER, 30, M, 3",
            "EARLY_ONSET, 30, F, 7",
            "IN_DANGER, 30, F, 4",
    })
    void calculateRisk(RiskEnum expected, int age, GenderEnum genderEnum, Integer nbTriggerTerms) {
        // Given
        // When
        RiskEnum result = RiskCalculator.calculate(age, genderEnum, nbTriggerTerms);
        // Then
        assertThat(result).isEqualTo(expected);
    }
}
