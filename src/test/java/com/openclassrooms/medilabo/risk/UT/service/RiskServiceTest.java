package com.openclassrooms.medilabo.risk.UT.service;

import com.openclassrooms.medilabo.patient.model.PatientResponse;
import com.openclassrooms.medilabo.risk.model.RiskEnum;
import com.openclassrooms.medilabo.risk.service.business.RiskService;
import com.openclassrooms.medilabo.risk.service.tech.NoteApiService;
import com.openclassrooms.medilabo.risk.service.tech.PatientApiService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RiskServiceTest {

    @Mock
    private PatientApiService patientApiServiceMock;

    @Mock
    private NoteApiService noteApiServiceMock;

    @InjectMocks
    private RiskService riskService;

    @Test
    void getNoneRisk(){
        // Given
        LocalDate birthday = LocalDate.now().minusYears(54);
        PatientResponse patientResponse = new PatientResponse();
        patientResponse.setBirthday(birthday);
        patientResponse.setGender("F");

        when(patientApiServiceMock.getPatient(1L)).thenReturn(patientResponse);
        when(noteApiServiceMock.getNotes(1L)).thenReturn(List.of(
                "Le patient déclare qu'il 'se sent très bien'. Poids égal ou inférieur au poids recommandé."
        ));
        // When
        RiskEnum risk = riskService.getRisk(1L);
        // Then
        assertThat(risk).isEqualTo(RiskEnum.NONE);
    }

    @Test
    void getBorderlineRisk() {
        // Given
        LocalDate birthday = LocalDate.now().minusYears(79);
        PatientResponse patientResponse = new PatientResponse();
        patientResponse.setBirthday(birthday);
        patientResponse.setGender("M");

        when(patientApiServiceMock.getPatient(2L)).thenReturn(patientResponse);
        when(noteApiServiceMock.getNotes(2L)).thenReturn(List.of(
                "Le patient déclare qu'il ressent beaucoup de stress au travail. Il se plaint également que son audition est anormale dernièrement.",
                "Le patient déclare avoir fait une réaction aux médicaments au cours des 3 derniers mois. Il remarque également que son audition continue d'être anormale."
        ));
        // When
        RiskEnum risk = riskService.getRisk(2L);
        // Then
        assertThat(risk).isEqualTo(RiskEnum.BORDERLINE);
    }

    @Test
    void getInDangerRisk() {
        // Given
        LocalDate birthday = LocalDate.now().minusYears(19);
        PatientResponse patientResponse = new PatientResponse();
        patientResponse.setBirthday(birthday);
        patientResponse.setGender("M");

        when(patientApiServiceMock.getPatient(3L)).thenReturn(patientResponse);
        when(noteApiServiceMock.getNotes(3L)).thenReturn(List.of(
                "Le patient déclare qu'il fume depuis peu.",
                "Le patient déclare qu'il est fumeur et qu'il a cessé de fumer l'année dernière. Il se plaint également de crises d’apnée respiratoire anormales. Tests de laboratoire indiquant un taux de cholestérol LDL élevé."
        ));
        // When
        RiskEnum risk = riskService.getRisk(3L);
        // Then
        assertThat(risk).isEqualTo(RiskEnum.IN_DANGER);
    }

    @Test
    void getEarlyOnsetRisk() {
        // Given
        LocalDate birthday = LocalDate.now().minusYears(21);
        PatientResponse patientResponse = new PatientResponse();
        patientResponse.setBirthday(birthday);
        patientResponse.setGender("F");

        when(patientApiServiceMock.getPatient(4L)).thenReturn(patientResponse);
        when(noteApiServiceMock.getNotes(4L)).thenReturn(List.of(
                "Le patient déclare qu'il lui est devenu difficile de monter les escaliers. Il se plaint également d’être essoufflé. Tests de laboratoire indiquant que les anticorps sont élevés. Réaction aux médicaments.",
                "Le patient déclare qu'il a mal au dos lorsqu'il reste assis pendant longtemps.",
                "Le patient déclare avoir commencé à fumer depuis peu. Hémoglobine A1C supérieure au niveau recommandé.",
                "Taille, Poids, Cholestérol, Vertige et Réaction."
        ));
        // When
        RiskEnum risk = riskService.getRisk(4L);
        // Then
        assertThat(risk).isEqualTo(RiskEnum.EARLY_ONSET);
    }


}